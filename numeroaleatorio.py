# -*- UTF-8 -*-
# Classe Circulo

import os

class NumeroAleatorio(object):


    def __init__(self, casasDecimais,  inicio, fim):

        self.casaDecimais = casasDecimais;
        self.inicio = inicio
        self.fim = fim

        self.inicioMult = self.inicio * 10 ** casasDecimais
        self.fimMult = self.fim * 10 ** casasDecimais

    def __str__(self):
        return "NumeroAleatorio => de : %f a %f com %d casas decimais" % (self.inicio, self.fim, self.casaDecimais)

    def getNext(self):
        val = int.from_bytes(os.urandom(10),'big',signed=False)
        val = val % abs(self.fimMult-self.inicioMult)
        return (val + self.inicioMult) / 10 ** self.casaDecimais