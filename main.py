# -*- UTF-8 -*-
# Módulo main

from ponto import Ponto
from quadrado import Quadrado
from circulo import Ciculo
from numeroaleatorio import NumeroAleatorio
from math import sqrt
from datetime import datetime
from doublequeue import DoubleQueue

print ("Iniciando processamento...")


## Define o raio dos círculos que serão usados no problema
raioPadrao = 1.0

## Define os pontos dos 3 Circulos
p1 = Ponto(0, 0)
p2 = Ponto(raioPadrao, 0)
p3 = Ponto(raioPadrao/2,sqrt( raioPadrao**2 - (raioPadrao/2)**2 ) )

print ("Criando 3 pontos: [ %s, %s, %s ]" % (p1, p2, p3) )

## Cria os 3 Círculos - Com o raio padrão.
c1 = Ciculo(p1, raioPadrao)
c2 = Ciculo(p2, raioPadrao)
c3 = Ciculo(p3, raioPadrao)

print ("Criando os 3 Círculos: [ %s, %s, %s ]" % (c1, c2, c3))

## Cria um retangulo com a posição e tamanho suficiente para acomodar os 3 Circulos.
quadrado = Quadrado(raioPadrao)

print ("Criando Quadrado onde será acomodado os 3 círculos: [ %s ]" % quadrado)

## Gera os responsáveis por gerar os números aleatórios (Usa o /dev/urandom do linux/mac)
numeroAleatorioX = NumeroAleatorio(8, quadrado.minX, quadrado.maxX)
numeroAleatorioY = NumeroAleatorio(8, quadrado.minY, quadrado.maxY)

## Quantidade de execuções
execucoes = 1500000

medias = 10;

## Inicia os contadores de acerto e exibição.
acertos = 0
exibeResultado = 10

ultimosValores = DoubleQueue(50)

for i in range(execucoes):

    p = Ponto(numeroAleatorioX.getNext(),numeroAleatorioY.getNext())
    c1_r = c1.estaNoCirculo(p)
    c2_r = c2.estaNoCirculo(p)
    c3_r = not c3.estaNoCirculo(p)


    if ( c1_r and c2_r and c3_r )  :
        acertos = acertos + 1

    if i % 100 or exibeResultado == i:
        areaAtual = float(3 * quadrado.calculaArea()*(acertos/i))
        ultimosValores.push(areaAtual)

    if(exibeResultado == i):
        print ("[%s] - Execução = %d\tAcertos = %d\tArea Estimada = %f" % (datetime.now(), i, acertos, areaAtual) )
        exibeResultado = exibeResultado + 15000


print ("[%s] - Execução = %d\tAcertos = %d\tArea Estimada = %f" % (datetime.now(), execucoes, acertos, float(3 * quadrado.calculaArea()*(acertos/execucoes))) )

print("Últimos Valores ", ultimosValores , "\nMédias => " , ultimosValores.media())