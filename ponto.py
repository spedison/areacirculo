# -*- UTF-8 -*-
# Classe Ponto

class Ponto(object):

    def __init__(self, x, y):
        self.x = x;
        self.y = y;

    def __str__(self):
        return "Ponto :: X = %f, Y = %f" % (self.x, self.y);
