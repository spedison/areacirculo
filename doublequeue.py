# -*- UTF-8 -*-

# Modulo de Helper para arrays

from array import array

class DoubleQueue  :

    def __init__(self, size):

        self.size = size
        self.currentPos = 0;
        self.data = array('f')

        for i in range(0, self.size):
            self.data.append(0.0)

    def push(self, val):
        self.data[ abs(self.currentPos) % self.size ] = val
        self.currentPos = self.currentPos + 1

    def pop(self):
        self.currentPos = abs(self.currentPos -1) % self.size
        data = self.data[self.currentPos]

    def media (self):

        soma = 0
        for i in self.data:
            soma = soma + i
        return soma / len (self.data)

    def __str__(self):
        return "Array de Tamanho: %d %s" % (self.size, self.data)

    #
    # def buscaMaior(self, arr):
    #     item = arr[0]
    #     pos = 0;
    #     for i in range(1,len(arr)-1):
    #         if(arr[i] > item):
    #             item = arr[i]
    #             pos = i
    #     return pos
    #
    # def buscaMenor(self, arr):
    #     item = arr[0]
    #     pos = 0
    #     for i in range(1,len(arr)-1):
    #         if(arr[i] < item):
    #             item = arr[i]
    #             pos = i
    #     return pos
    #
    # def buscaMaiorTrocaSeMenor(self, arr, valor):
    #     pos = self.buscaMaior(arr)
    #     if(arr[pos] < valor):
    #         arr[pos] = valor
    #
    # def buscaMaiorTrocaSeMaior(self, arr, valor):
    #     pos = self.buscaMaior(arr)
    #     if(arr[pos] > valor):
    #         arr[pos] = valor
    #
    # def buscaMenorTrocaSeMaior(self, arr, valor):
    #     pos = self.buscaMenor(arr)
    #     if(arr[pos] > valor):
    #         arr[pos] = valor
    #
    # def buscaMenorTrocaSeMenor(self, arr, valor):
    #     pos = self.buscaMenor(arr)
    #     if(arr[pos] < valor):
    #         arr[pos] = valor