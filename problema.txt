Kevin Wang
Academic Director at Areteem Institute / Math Zoom Academy

Three circles:
The radii of the three circles in the figure are all 1, and each circle passes through the centers of the other two circles. Find the total area of the shaded region.

Três círculos:
Os raios dos três círculos na figura são todos 1, e cada círculo passa pelos centros dos outros dois círculos. Encontre a área total da região sombreada.
