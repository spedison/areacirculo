# -*- UTF-8 -*-

# Classe Quadrado

from math import sqrt

class Quadrado:

    def __init__(self, raio):
        raio = abs(raio)
        self.minX = 0
        self.maxX = raio
        self.minY = -1 * raio
        self.maxY = 0

    def __str__(self):
        return "Quadrado :: minX = %f - minY = %f - maxX = %f - maxY = %f" % (self.minX, self.minY, self.maxX, self.maxY)

    def calculaArea(self):
        return ( self.maxY - self.minY ) * ( self.maxX - self.minX )