# -*- UTF-8 -*-
# Classe Circulo

from ponto import Ponto

class Ciculo(object):


    def __init__(self, ponto,  raio):

        self.ponto = ponto;
        self.raio = raio;

    def __str__(self):
        return "Circulo :: Ponto = %s, Raio =%f" % (self.ponto, self.raio)

    def estaNoCirculo(self, ponto):

        deltaY = abs(self.ponto.y - ponto.y)
        deltaX = abs(self.ponto.x - ponto.x)
        dist = (deltaX**2 + deltaY**2)**0.5

        return dist <= self.raio

